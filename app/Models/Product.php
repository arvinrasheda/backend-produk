<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    protected $primaryKey = 'id';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nama',
        'jumlah',
        'harga',
        'path_foto',
        'status',
        'berat',
    ];

    /**
     * @param $data
     * @return $this
     */
    public function insert($data): Product
    {
        $this->fill($data);
        $this->save();
        return $this;
    }

    /**
     * @param $id
     * @return bool
     */
    public function buyProduct($id): bool
    {
        $found = $this->where('id', $id)->first();

        if ($found) {
            $tmpCountJumlah = $found->jumlah;
            if ($tmpCountJumlah > 1) {
                $tmpCountJumlah = $tmpCountJumlah - 1;
                $status = 1;
            } else {
                $tmpCountJumlah = $tmpCountJumlah - 1;
                $status = 0;
            }

            $this->where('id', $id)->update([
                'jumlah' => $tmpCountJumlah,
                'status' => $status
            ]);
            return true;
        }
        return false;
    }
}
