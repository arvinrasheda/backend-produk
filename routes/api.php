<?php

use App\Models\ClaimedCharacterName;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('product/list', function(){
    $data = Product::where('status', 1)->get();
    $result = [];
    foreach ($data as $key => $value) {
        $result[] = [
            'id' => $value->id,
            'nama' => $value->nama,
            'jumlah' => $value->jumlah,
            'status' => $value->status,
            'berat' => $value->berat,
            'harga' => $value->harga,
            'image' => url('storage/' . $value->path_foto)
        ];
    }
    return $result;
});

Route::get('product/detail', function(Request $request){
    $id = $request->get("id");
    $data = Product::where('id', $id)->first()->toArray();
    $data['image'] = url('storage/' . $data['path_foto']);
    return $data;
});

Route::post('product/store', function(Request $request){
    $input = $request->all();

    $file = $request->file('image');

    $fileName = $input['nama'].'.'.$file->getClientOriginalExtension();

    $path= date('Ym').'/'.$fileName;
    Storage::disk('public')->putFileAs(date('Ym'), $file, $fileName);

    DB::beginTransaction();
    try {
        (new App\Models\Product)->insert([
            'nama' => $input['nama'],
            'jumlah' => $input['jumlah'],
            'harga' => $input['harga'],
            'path_foto' => $path,
            'berat' => $input['berat'],
            'status' => 1,
        ]);
        $code = 200;
        $data = "Create Product Success";
        DB::commit();
    }  catch (Exception $e) {
        DB::rollback();
        $data = "Create Product Error";
        $code = 500;
    }

    return [
        'data' => $data,
        'code' => $code,
    ];
});

Route::post('product/buy', function(Request $request){
    $input = $request->all();
    try {
        (new App\Models\Product)->buyProduct($input['id']);
        $code = 200;
        $data = "Buy Product Success";
        DB::commit();
    }  catch (Exception $e) {
        DB::rollback();
        $data = "Buy Product Error";
        $code = 500;
    }

    return [
        'data' => $data,
        'code' => $code,
    ];
});


